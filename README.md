# Map Utilities from Common Data Factory 
## Based on Common Ground Design system

The map styles in this repository are based on the [Common ground Design system](https://commonground.gitlab.io/core/design-system/). Colors, fonts and icons are inspired and conform this design system. 

The map tiles are derived from the [OpenMapTiles](https://openmaptiles.org) project. So setting up your own map with these utils requires the Openmaptiles stack. 


#### How this repro works

In every folder you will find the `/dev` and `/publish` directory. 

* `/dev` is contains all developing and working files. As well as a description how to develop the sprites or glyphs. 

* `/publish` contains all final files as published under:

    https://commondatafactory.nl/mapdesing/

These files can be accesed online and re-used in other applications. 




