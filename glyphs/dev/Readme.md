# Developing Glyphs

Running node 10. 

0. Get node-fontnik  https://github.com/mapbox/node-fontnik

    wget https://github.com/mapbox/node-fontnik/archive/master.zip
    unzip master.zip
    cd node-fontnik-master
    npm install -g fontnik

1. Download fonts
2. Make glyphs directorys
3. Build glyphs

    build-glyphs fonts/source-sans-pro/SourceSansPro-Regular.otf glyphs/SourceSansPro-Regular



## Naming Glyphs

Naming convention for glyphs: CamelCase for the font. Dash . Camelcase font variety. 

    source-sans-pro-regular -> SourceSansPro
    source-sans-pro bold -> SourceSansPro-Bold
    source-sans-pro bold italic -> SourceSansPro-BoldIt


## Tutorial 
Tutorial op https://developer.tomtom.com/maps-sdk-web/tutorials-use-cases/my-company-location