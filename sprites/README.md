# Sprites

Acces url: 

    "sprite": "https://commondatafactory/sprite/cdfSprite_v1",

For every sprite a normal and retina (`@2x`) version are made. MapboxGL chooses these automatically, but expects both to be present at the sprite endpoint. 


#### Versioning

When new icons are added to the sprite the same version number is used. 
When icons are removed or the naming changed a new version number is assigned. 