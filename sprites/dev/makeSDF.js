var fs = require("fs");
var custom_variable = process.argv.slice(2);

if (custom_variable.length != 1) {
    console.error("Geen of niet voldoende parameters ingevoerd! Geef een bestandsnaam en x en y waarden. Bijvoorbeeld: \n  sprite.json")
}
else if (custom_variable.length == 1) {
    let file = custom_variable[0];
    console.log("Overwriting File: " + file +"\n Making SDF sprite")
    makeSDF(file);
}

function makeSDF(file){
    let data = fs.readFileSync(file, 'utf8');
    let dataparse = JSON.parse(data);
    Object.keys(dataparse).map(d => {
        dataparse[d].sdf = true;
    })
    let jsonFile = JSON.stringify(dataparse);
    fs.writeFileSync(file, jsonFile, 'utf8');
}
