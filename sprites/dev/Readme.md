# Sprite Development

1. Download svg icons 
    https://remixicon.com/

2. improve svgs 

    npm install -g svgo
    svgo -f svg_download -o svg_improved --pretty --enable=removeXMLNS --disable=convertPathData  --datauri=utf8  -r 

3. sprite zero

Make sure to use node version 7. 
    
    npm install spritezero -g
    spritezero sprites/cdfSprite_v1 svg_improved
    spritezero --retina sprites/cdfSprite_v1@2x svg_improved 

4. Make sdf sprite

    node makeSDF.js sprites/cdfSprite_v1@2x.json 
    node makeSDF.js sprites/cdfSprite_v1.json 

